import React from "react";
import Navbar from "../components/Navbar";
import Badge from "../components/Badge";
import BadgeForm from "../components/BadgeForm";

import header from "../images/badge-header.svg";
class BadgeNew extends React.Component {
  state = {
    form: {
      fullName: "",
      email: "",
      ocupation: ""
    }
  };
  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };
  render() {
    return (
      <div>
        <Navbar />
        <div className="banner">
          <img src={header} alt="" />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <Badge
                title={this.state.form.fullName}
                ocupation={this.state.form.ocupation}
                email={this.state.form.email}
              />
            </div>
            <div className="col-md-8">
              <BadgeForm
                onChange={this.handleChange}
                formBadge={this.state.form}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default BadgeNew;
