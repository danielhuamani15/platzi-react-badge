import React from "react";
import Navbar from "../components/Navbar";
import header from "../images/badge-header.svg";
class Badges extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="banner">
          <img src={header} alt="" />
        </div>
        <div className="badge__container container">
          <div className="badge__container__button ">
            <a href="" className="btn btn-primary">
              Add Badge
            </a>
          </div>
        </div>
      </div>
    );
  }
}
export default Badges;
