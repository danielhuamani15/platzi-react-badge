import React from "react";

class BadgeForm extends React.Component {
  handleClick = e => {
    console.log("buttonClick");
  };
  handleSubmit = e => {
    e.preventDefault();
    console.log("Submit", this.state);
  };
  render() {
    return (
      <div className="form_badge">
        <h1 className="form_badge__title">New Attendent</h1>
        <form method="POST" action="" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="">First Name</label>
            <input
              onChange={this.props.onChange}
              type="text"
              className="form-control"
              name="fullName"
              value={this.props.formBadge.fullName}
            />
          </div>
          <div className="form-group">
            <label htmlFor="">Ocupation</label>
            <input
              onChange={this.props.onChange}
              type="text"
              className="form-control"
              name="ocupation"
              value={this.props.formBadge.ocupation}
            />
          </div>
          <div className="form-group">
            <label htmlFor="">Email</label>
            <input
              onChange={this.props.onChange}
              type="text"
              className="form-control"
              name="email"
              value={this.props.formBadge.email}
            />
          </div>
          <button type="submit" className="btn btn-success">
            Save
          </button>
        </form>
      </div>
    );
  }
}
export default BadgeForm;
