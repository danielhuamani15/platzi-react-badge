import React from "react";
import Logo from "../images/logo.svg";
class Navbar extends React.Component {
  render() {
    return (
      <div className="nav">
        <div className="container">
          <a href="/" className="d-flex align-items-center ">
            <img src={Logo} alt="" />
            <h2 className=" logo__title">Conference</h2>
          </a>
        </div>
      </div>
    );
  }
}
export default Navbar;
