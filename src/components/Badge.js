import React from "react";
import Logo from "../images/badge-header.svg";

class Badge extends React.Component {
  render() {
    const title = "Daniel Huamani";
    const ocupation = "Desarrollador fullstack";

    return (
      <div>
        <h2 className="title text-center mt-4">{this.props.title}</h2>
        <h4 className="title text-center mt-4">{this.props.ocupation}</h4>
        <h5 className="title text-center mt-4">{this.props.email}</h5>
      </div>
    );
  }
}
export default Badge;
